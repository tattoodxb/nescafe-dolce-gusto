const path = require('path');
const glob = require('glob');
const globall = require('glob-all')
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
//const ExtractTextPlugin = require('extract-text-webpack-plugin'); //webpack4 bug
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
//const PurifyCSSPlugin = require('purifycss-webpack');
const whitelister = require('purgecss-whitelister')
const PurgecssPlugin = require('purgecss-webpack-plugin');

/*const { ImageminWebpackPlugin } = require("imagemin-webpack");
const imageminGifsicle = require("imagemin-gifsicle");
const imageminOptipng = require('imagemin-optipng');
const imageminJpegtran = require('imagemin-jpegtran');*/

const ImageminPlugin = require( 'imagemin-webpack-plugin' ).default;
const { ImageminWebpackPlugin } = require("imagemin-webpack");
var imageminMozjpeg = require('imagemin-mozjpeg');
var imageminJpegtran = require('imagemin-jpegtran');
var imageminPngquant = require('imagemin-pngquant');
var imageminOptipng = require('imagemin-optipng');
var imageminGifsicle = require("imagemin-gifsicle")
var imageminSvgo = require('imagemin-svgo');

const HtmlWebpackInlineSVGPlugin = require('html-webpack-inline-svg-plugin');

const CopyWebpackPlugin = require('copy-webpack-plugin')

const DEV_MODE = process.env.NODE_ENV === 'dev';

const PATHS = {
  src: path.join(__dirname, 'src')
}

const DIRFont = path.resolve(__dirname, "./src/fonts/");

const HtmlCriticalWebpackPlugin = require("html-critical-webpack-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

//var buildPath = '/wp-content/themes/dubai-association-centre-theme/';
function collectWhitelistCustom() {
  // do something to collect the whitelist
  var group1 = whitelister(['layouts/custom-styles.scss','../../plugins/contact-form-7/includes/css/styles.css']);
  var group2 = ['collapsing','show','hide','explicit','open','close','*-visible','visible-*','modal','modal-backdrop','no-gutters','vc_column_container','vc_column-inner','wpb_wrapper','alert','attachment-full','size-thumbnail','lazyloading','lazyload','lazyloaded'];
  var complete = group1.concat(group2); 
  return complete;
}

var bjJS = "../plugins/bj-lazy-load/js/bj-lazy-load.min.js"; // use to exclude js from minify in babel-loader
var themecustomJS = "../themes/dubai-association-centre-theme/js/custom.js";

module.exports = {
  //devtool: DEV_MODE ? 'eval' : 'source-map',
  entry: {
    common: path.resolve(__dirname, 'src/js/common.js'),
    home: path.resolve(__dirname, 'src/js/home.js'),
    about: path.resolve(__dirname, 'src/js/about.js'),

    upcoming_events: path.resolve(__dirname, 'src/js/upcoming-events.js'),
    past_events: path.resolve(__dirname, 'src/js/upcoming-events.js'),
    research: path.resolve(__dirname, 'src/js/upcoming-events.js'),
    press: path.resolve(__dirname, 'src/js/upcoming-events.js'),
    ambassadors: path.resolve(__dirname, 'src/js/upcoming-events.js'),

    faqs: path.resolve(__dirname, 'src/js/faqs.js'),

    search: path.resolve(__dirname, 'src/js/search.js'),

    single: path.resolve(__dirname, 'src/js/single.js'),

    event: path.resolve(__dirname, 'src/js/event.js'),
    knowledge_hub: path.resolve(__dirname, 'src/js/event.js'),
    our_services: path.resolve(__dirname, 'src/js/event.js'),

    licensing_requirements: path.resolve(__dirname, 'src/js/licensing.js'),
    benefits_and_services: path.resolve(__dirname, 'src/js/benefits-and-services.js'),
    application_process: path.resolve(__dirname, 'src/js/registration.js'),
    contact: path.resolve(__dirname, 'src/js/contact.js'),
    login: path.resolve(__dirname, 'src/js/login.js'),

    lostpassword: path.resolve(__dirname, 'src/js/lostpassword.js'),
    resetpass: path.resolve(__dirname, 'src/js/lostpassword.js'),
    
    dashboard: path.resolve(__dirname, 'src/js/dashboard.js'),
    event_subscriptions: path.resolve(__dirname, 'src/js/dashboard.js'),
    dashboard_forum: path.resolve(__dirname, 'src/js/dashboard.js'),
    dashboard_topics: path.resolve(__dirname, 'src/js/dashboard.js'),
    dashboard_forum_single: path.resolve(__dirname, 'src/js/dashboard.js'),
    dashboard_settings: path.resolve(__dirname, 'src/js/dashboard.js'),
    
    //'font-awesome-sass-loader!./font-awesome.config.js', //old
    //"font-awesome-webpack!./font-awesome.config.js", updated
  },
  output: {
    //path: path.resolve(__dirname, 'dist'),
    path: path.resolve(__dirname, 'dist'),
    //publicPath: 'http://dac-centre-webpack.tattoo360.net/wp-content/themes/dubai-association-centre-theme/dist/', // This is used to generate URLs to e.g. images
    publicPath: 'http://10.160.6.61/dubai-association-centre/web/wp-content/themes/dubai-association-centre-theme/dist/',
    //publicPath: '', //for local run
    //publicPath: 'http://localhost:8888/dubai-association-centre/web/wp-content/themes/dubai-association-centre-theme/dist/', //local own mac
    filename: 'js/[name].bundle.js',
    
    //publicPath: buildPath
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        //exclude: [/node_modules/, /themecustomJS/], //multiple exclude
        //exclude: [/node_modules/],
        include: [
          path.resolve(__dirname, "src")
        ],
        /*exclude: [
          /node_modules/,
          path.resolve(__dirname, "js")
        ],*/
        loader: 'babel-loader'
      },
      /*{
        test: require.resolve('jquery'),
        use: [{
          loader: 'expose-loader',
          options: 'jQuery'
        },{
          loader: 'expose-loader',
          options: '$'
        }]
      },
      {
        test: require.resolve('tether'),
        use: [
          { loader: 'expose-loader', options: 'Tether' }
        ]
      },*/
      DEV_MODE ? {
        test: /\.(scss|css)$/,
        use: [{
          loader: 'style-loader', // inject CSS to page
        }, {
          loader: 'css-loader', // translates CSS into CommonJS modules
        }, {
          loader: 'postcss-loader', // Run post css actions
          options: {
            plugins: function () { // post css plugins, can be exported to postcss.config.js
              return [
                require('precss'),
                require('autoprefixer')
              ];
            }
          }
        }, {
          loader: 'sass-loader' // compiles Sass to CSS
        }]
      }
      : 
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: true,
              publicPath: '../'
            }
          },
        ]
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false,
              publicPath: '../'
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              url: false,
              publicPath: '../'
            }
          },
          {
            loader: 'sass-loader',
            options: {
              url: false,
              publicPath: '../'
            }
          }
        ]
      },
      /*{
        test: /\.less$/,
        loader: 'less-loader' // compiles Less to CSS
      },*/
      //font
      /*{
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              limit: 10000,
              mimetype: 'application/font-woff',
              name: "[name].[ext]",
              outputPath: 'fonts/',
              publicPath: 'fonts/',
              include: path.join(__dirname,'src')
            }
          }
        ]
      },
      {
        test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'fonts/',
              publicPath: 'fonts/',
              name: "[name].[ext]",
              include: path.join(__dirname,'src')
            }
          }
        ]
      },*/
      {
        test: /\.(jpe?g|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/, 
        include: [
          path.resolve(__dirname, 'src/assets/fonts')
       ],
        loader: 'file-loader?name=[name].[ext]'
     },
       {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          //'file-loader?name=images/[name].[ext]',
          //'file-loader?name=[name].[ext]&outputPath=assets/images/&publicPath=assets/images/',
          {
            loader: 'file-loader',
            options: {
              limit: 10000,
              outputPath: 'assets/images/',
              publicPath: 'assets/images/',
              name: '[name].[ext]',
            },
          },
         /*{
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                progressive: true,
                quality: 65
              },
              svgo:{
                plugins: [
                  {
                    removeViewBox: false
                  },
                  {
                    removeEmptyAttrs: false
                  }
                ]
              },
              // optipng.enabled: false will disable optipng
              optipng: {
                enabled: false,
              },
              pngquant: {
                quality: '65-90',
                speed: 4
              },
              gifsicle: {
                interlaced: false,
              },
              // the webp option will enable WEBP
              webp: {
                quality: 75
              }
            }
         },*/
        ],
      },

    ]
  },
  resolve: {
    extensions: ['.js','.scss','.css'],
    alias: {
      'img': path.join(__dirname, 'src', 'assets/images'),
      'fontsDIR': path.resolve(__dirname, 'src/assets/fonts'),
      'scss': path.resolve(__dirname, 'src/scss'),
      'vendor': path.resolve(__dirname, 'src/js/components/vendor'),
      'layouts': path.resolve(__dirname, 'layouts'),
      'wp_js': path.resolve(__dirname, 'js'),
      'wp_plugins': path.resolve(__dirname, '../../plugins'),
      '@fortawesome/fontawesome-free-solid$': '@fortawesome/fontawesome-free-solid/shakable.es.js'
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Tether: 'tether'
    }),

    new HtmlWebPackPlugin({
      title: 'Custom template',
      template: path.join(__dirname, 'src', 'index.html'),
      /*minify: {
        collapseWhitespace: DEV_MODE ? false :  true
      },*/
      //excludeChunks: ['about'],
      chunks: ['home'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'About template',
      filename: 'about.html',
      template: path.join(__dirname, 'src', 'about.html'),
      chunks: ['about'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Upcoming Events template',
      filename: 'upcoming-events.html',
      template: path.join(__dirname, 'src', 'upcoming-events.html'),
      chunks: ['upcoming_events'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Past Events template',
      filename: 'past-events.html',
      template: path.join(__dirname, 'src', 'past-events.html'),
      chunks: ['past_events'],
      hash: true,
    }),

    new HtmlWebPackPlugin({
      title: 'Events template',
      filename: 'event.html',
      template: path.join(__dirname, 'src', 'event.html'),
      chunks: ['event'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'knowledge Hub template',
      filename: 'knowledge-hub.html',
      template: path.join(__dirname, 'src', 'knowledge-hub.html'),
      chunks: ['knowledge_hub'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Our Services template',
      filename: 'our-services.html',
      template: path.join(__dirname, 'src', 'our-services.html'),
      chunks: ['our_services'],
      hash: true,
    }),

    new HtmlWebPackPlugin({
      title: 'Press template',
      filename: 'press.html',
      template: path.join(__dirname, 'src', 'press.html'),
      chunks: ['press'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Research template',
      filename: 'research.html',
      template: path.join(__dirname, 'src', 'research.html'),
      chunks: ['research'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Search template',
      filename: 'search.html',
      template: path.join(__dirname, 'src', 'search.html'),
      chunks: ['search'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Ambassadors template',
      filename: 'ambassadors.html',
      template: path.join(__dirname, 'src', 'ambassadors.html'),
      chunks: ['ambassadors'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Faqs template',
      filename: 'faqs.html',
      template: path.join(__dirname, 'src', 'faqs.html'),
      chunks: ['faqs'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Single template',
      filename: 'single.html',
      template: path.join(__dirname, 'src', 'single.html'),
      chunks: ['single'],
      hash: true,
    }),
    
    new HtmlWebPackPlugin({
      title: 'Licensing template',
      filename: 'licensing.html',
      template: path.join(__dirname, 'src', 'licensing.html'),
      chunks: ['licensing_requirements'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Benefits and Services template',
      filename: 'benefits-and-services.html',
      template: path.join(__dirname, 'src', 'benefits-and-services.html'),
      chunks: ['benefits_and_services'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Application Process template',
      filename: 'application-process.html',
      template: path.join(__dirname, 'src', 'application-process.html'),
      chunks: ['application_process'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Lost Password template',
      filename: 'lostpassword.html',
      template: path.join(__dirname, 'src', 'lostpassword.html'),
      chunks: ['lostpassword'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'resetpass template',
      filename: 'resetpass.html',
      template: path.join(__dirname, 'src', 'lostpassword.html'),
      chunks: ['resetpass'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Contact template',
      filename: 'contact.html',
      template: path.join(__dirname, 'src', 'contact.html'),
      chunks: ['contact'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Login template',
      filename: 'login.html',
      template: path.join(__dirname, 'src', 'login.html'),
      chunks: ['login'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Dashboard template',
      filename: 'dashboard.html',
      template: path.join(__dirname, 'src', 'dashboard.html'),
      chunks: ['dashboard'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Events Subscriptions template',
      filename: 'event-subscriptions.html',
      template: path.join(__dirname, 'src', 'event-subscriptions.html'),
      chunks: ['event_subscriptions'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Dashboard Forum template',
      filename: 'dashboard-forum.html',
      template: path.join(__dirname, 'src', 'dashboard-forum.html'),
      chunks: ['dashboard_forum'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Dashboard Topics template',
      filename: 'dashboard-forum-topics.html',
      template: path.join(__dirname, 'src', 'dashboard-forum-topics.html'),
      chunks: ['dashboard_topics'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Dashboard Forum Single template',
      filename: 'dashboard-forum-single.html',
      template: path.join(__dirname, 'src', 'dashboard-forum-single.html'),
      chunks: ['dashboard_forum_single'],
      hash: true,
    }),
    new HtmlWebPackPlugin({
      title: 'Dashboard Edit Profile template',
      filename: 'dashboard-forum-edit-profile.html',
      template: path.join(__dirname, 'src', 'dashboard-forum-edit-profile.html'),
      chunks: ['dashboard_settings'],
      hash: true,
    }),

    new UglifyJsPlugin({
      uglifyOptions: {
        cache: true,
        parallel: 8,
        ecma: 8,
        warnings: false,
        compress: {
          drop_console: true,
        },
        output: {
          comments: false,
          beautify: false,
        },
        nameCache: null,
        ie8: false,
      }
    }),
    
    new HtmlWebpackInlineSVGPlugin(), // needs to be after HtmlWebPackPlugin

    //new ExtractTextPlugin('css/styles.css'),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "css/[name].css",
      chunkFilename: "css/[name].css",
    }),
    // Make sure this is after ExtractTextPlugin!
    new PurgecssPlugin({
      //paths: glob.sync(`${PATHS.src}/**/*`, { nodir: true }),
      paths: globall.sync([
        path.resolve(__dirname, 'src/**/*.js'),
        path.resolve(__dirname, 'src/**/*.html'),
        path.resolve(__dirname, 'bbpress/**/*.php'),
        path.resolve(__dirname, 'inc/**/*.php'),
        path.resolve(__dirname, 'js/**/*.js'),
        path.resolve(__dirname, 'template-parts/**/*.php'),
        path.resolve(__dirname, '*.php'),
      ]),
      //only: ['home', 'about', 'upcoming_events', 'past_events', 'research', 'press', 'event', 'knowledge_hub', 'licensing_requirements', 'benefits_and_services', 'register', 'dashboard'],
      //whitelist: ['collapsing','show','hide','explicit','open','close','*-visible','visible-*','modal','modal-backdrop','no-gutters','vc_column_container','vc_column-inner','wpb_wrapper','alert'],
      keyframes:true,
      fontFace: true,
      whitelistPatterns: [/^(modal|swiper|alert)/],
      whitelistPatternsChildren: [/^(modal|swiper|alert)/],

       // `whiltelist` needed to ensure Typer classes stay in the bundle.
       //whitelist: whitelister('layouts/custom-styles.scss'), // works alone

       whitelist: collectWhitelistCustom
    }),

    // run only if home page or single pages has changes on the first fold
    /*new HtmlCriticalWebpackPlugin({
      base: path.resolve(__dirname, 'dist'),
      src: 'index.html',
      dest: 'index.html',
      inline: true,
      minify: true,
      extract: true,
      width: 414,
      height: 736,
      penthouse: {
        blockJSRequests: false,
      }
    }),

    new HtmlCriticalWebpackPlugin({
      base: path.resolve(__dirname, 'dist'),
      src: 'about.html',
      dest: 'about.html',
      inline: true,
      minify: true,
      extract: true,
      width: 414,
      height: 736,
      penthouse: {
        blockJSRequests: false,
      }
    }),*/
    
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src/assets/images/'),
        to: path.resolve(__dirname, 'assets/images/') //working in office
        //to: path.resolve(__dirname,'dist', 'assets/images/') //working on pc mac
      },
      {
        from: path.resolve(__dirname, 'src/assets/fonts/'),
        to: path.resolve(__dirname, 'assets/fonts/'), // working in office 
        //to: path.resolve(__dirname,'dist', 'assets/fonts/'), //working on pc mac
        test: /([^/]+)\/(.+)\.svg$/,
        ignore: [ '*.eot','*.otf','*.ttf','*.woff','*.woff2' ]
      },
      {
        from: path.resolve(__dirname, 'src/assets/fonts/'),
        to: path.resolve(__dirname, 'dist/assets/fonts/'), // working in office and pc mac
        ignore: [ '*.svg']
      },
    ]),

    new ImageminWebpackPlugin({
      name: "[path][name].[ext]",
      test: /\.(jpe?g|png|gif|svg)$/i,
      imageminOptions: {
        plugins: [
          imageminSvgo({
            plugins: [
              {
                removeViewBox: false
              },
              {
                removeTitle: true
              },
              {
                removeComments:true
              },
              {
                removeMetadata:true
              },
              {
                removeDesc:true
              },
              {
                removeEmptyAttrs:true
              },
              {
                cleanupIDs: true,
              },
              {
                addClassesToSVGElement: {
                  className: "svg-inline",
                },
              },
              {
                removeAttrs: {
                  attrs: 'svg:id'
                },
              }
            ]
          }),
          imageminGifsicle({
            interlaced: true,
            optimizationLevel: 3
          }),
          imageminOptipng({
            interlaced: true,
            optimizationLevel: 3
          }),
          imageminPngquant({
            quality: 100,
            speed: 4,
          }),
          imageminMozjpeg({
            quality: 65,
            progressive: true,
          }),
        ]
      },
    }),

    /*new CopyWebpackPlugin([
      { from: 'src/assets/images/', to: 'assets/images/' },
      { from: 'src/assets/fonts/', to: 'assets/fonts/' }
      
    ]),

    new ImageminPlugin({
      test: /\.(jpe?g|png|gif|svg)$/i,
      optipng: {
          optimizationLevel: 7,
      },
      pngquant: {
          quality: '65-90',
          speed: 4,
      },
      gifsicle: {
          optimizationLevel: 3,
      },
      svgo: {
          plugins: [{
              removeViewBox: false,
              removeEmptyAttrs: true,
          }],
      },
      jpegtran: {
          progressive: true,
      },
      plugins: [
          imageminMozjpeg({
              quality: 65,
              progressive: true,
          }),
      ],
  	}),
    */
  
  ]
}
