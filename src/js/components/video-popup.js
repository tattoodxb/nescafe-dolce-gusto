import 'scss/components/video-popup.scss';

  console.log("videoPopUp");

  var $videoSrc;
  $('.video-btn').click(function () {
    $videoSrc = $(this).data("src");
    $(".player").YTPPause();
  });
  console.log($videoSrc);



  // when the modal is opened autoplay it  
  $('#pop-video').on('shown.bs.modal', function (e) {
    
    var windowW = parseInt(jQuery(window).width());
    var windowH = parseInt($(window).height());	
    if(windowW>480){
      var vidH = windowH - 40;
      var vidW = (16/9) * vidH;
    }else{
      var vidW = windowW - 14;
      var vidH = (9 / 16) * vidW;
    }
    
    $("#pop-video .embed-responsive,#pop-video .modal-dialog").css({"max-width": vidW, "max-height":vidH});
    $( "#pop-video .embed-responsive" ).animate({
      width: vidW,
      height: vidH
    }, 300 );
  
    // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
    $("#video").attr('src', $videoSrc + "?rel=0&amp;showinfo=0&amp;modestbranding=1&amp;autoplay=1");
  })


  // stop playing the youtube video when I close the modal
  $('#pop-video').on('hide.bs.modal', function (e) {
    // a poor man's stop video
    $("#video").attr('src', $videoSrc);
    $(".player").YTPPlay();
  });