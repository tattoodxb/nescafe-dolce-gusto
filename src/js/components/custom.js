
var Ellipsis = require("bundle-loader?lazy&name=ellipsis!vendor/ellipsis");
Ellipsis(function(file){
    console.log("Ellipsis");
});


var validateForms = require("bundle-loader?lazy&name=validateForms!rootjs/validate-forms.js");
var validate = require("bundle-loader?lazy&name=validate!vendor/jquery-validate/jquery.validate");
validate(function(file){
  console.log("validate");
  validateForms(function(file){
    console.log("validateForms");
  });
});

var matchHeight = require("bundle-loader?lazy&name=matchHeight!vendor/jquery.matchHeight");
matchHeight(function(file){
  var windowW = parseInt(jQuery(window).width());

  
  if(windowW>480){
    //matchCallback();
    if($(".calendar-outer-container").length>0){
      console.log("matchHeight for calendar");
      MatchCalendarParentH()
    }
    matchH();
  }
  
});


function setveil(){
  $( ".unveil" ).each(function() {
    var el = $(this);
    
    var parent = el.parent();
    var child = el.children().first();
    var outerH = el.height();
    var innerH = child.height();
    
    parent.attr("data-height",outerH).css("height",outerH);
    el.attr("data-height",outerH).css("height","0");
    child.attr("data-height",innerH).css("height",innerH);
    

    console.log("------ setveil --------------");
    console.log(el);
    console.log(parent);
    console.log(child);
    console.log(outerH);
    console.log(innerH);
  });
}
setveil();

function unveil(){
  $( ".current .unveil[data-direction='ttb']" ).each(function() {
    console.log("------ unveil --------------");
    var el = $(this);
    var H = el.attr("data-height");
    console.log(H);
  });
}


function matchCallback(){
  console.log("matchCallback");
  $.fn.matchHeight._afterUpdate = function(event, groups) {
    console.log("_afterUpdate");
    var windowW = parseInt(jQuery(window).width());
    var groupEl =  groups[0].elements;
    if(windowW>480){
      groupEl.each(function(){
        console.log("groupEl.each");
        var target = $(this);
        if(target.hasClass("calendar-outer-container")){
          console.log("Has Class");
          //console.log(target.attr("class"));
          MatchcalendarH(target);
        }else{
          console.log("No Class");
        }
      })
    }
  }
}

function matchH(){
  console.log("match height generic");
  
  $(".match .match-item").matchHeight({
    byRow: true,
    property: 'height',
    target: null,
    remove: false
  });
  
}

function MatchCalendarParentH(){
  console.log("MatchCalendarParentH");
  
  $('.height-wr .height-target').matchHeight({
    byRow: true,
    property: 'height',
    target: $('.height-wr .height-target.calendar-outer-container'),
    remove: false
  });
  
}

function MatchcalendarH(target){
  console.log("MatchcalendarH")
  var heightSource = target.outerHeight();
  var Hminus = parseInt($(".resources-container .effect-goliath").css("margin-bottom"))*2;
  var finalH = (heightSource - Hminus) / 2;
  console.log(heightSource);
  console.log(Hminus);
  console.log(finalH);
  $(".resources-container .effect-goliath figure").css("height",finalH)
}

function is_touch_device() {
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    var mq = function(query) {
      return window.matchMedia(query).matches;
    }
  
    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
      console.log("ontouchstart");
      $("html").addClass("touch")
      return true;
    }
  
    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://git.io/vznFH
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
  }

  is_touch_device();

  /*function matchH() {
    var partnerH = jQuery('.height-wr .height-target').map(function() {
        return jQuery(this).outerHeight();
    }).get();
    var partnermaxH = Math.max.apply(null, partnerH);
    jQuery('.height-wr .height-target').height(partnermaxH);
  }
  matchH()*/

  $.fn.slideFadeToggle = function(speed, easing, callback) {
    return this.animate({
        opacity: 'toggle',
        height: 'toggle'
    }, speed, easing, callback);
  };
  $.fn.slideFadeIn = function(speed, easing, callback) {
    return this.animate({
        opacity: 'show',
        height: 'show'
    }, speed, easing, callback);
  };
  $.fn.slideFadeOut = function(speed, easing, callback) {
    return this.animate({
        opacity: 'hide',
        height: 'hide'
    }, speed, easing, callback);
  };

  $.fn.animateAuto = function(prop, speed, callback){
    var elem, height, width;
    return this.each(function(i, el){
        el = $(el), elem = el.clone().css({"height":"auto","width":"auto"}).appendTo("body");
        height = elem.css("height"),
        width = elem.css("width"),
        elem.remove();
        
        if(prop === "height")
            el.animate({"height":height}, speed, callback);
        else if(prop === "width")
            el.animate({"width":width}, speed, callback);  
        else if(prop === "both")
            el.animate({"width":width,"height":height}, speed, callback);
    });  
}