import 'scss/components/hero-banner.scss';

var Custom = require("bundle-loader?lazy&name=custom!./custom");
Custom(function (file) {
  console.log("custom - from hero banner");
  heroBannerNow();
});
//click for video
function heroBannerNow() {

  //lets go button
  $(".js-banner-trigger-show").on('click', function (event) {
    var windowW = parseInt(jQuery(window).width());
    
    $(".hero-overlay-bg").removeClass("page-hidden").css("z-index", "0");
    $(".overlay-content-bg").fadeOut(1000);
    $(".overlay-content .context-wr,.overlay-content .btn-primary,.overlay-content .arrow-wr").fadeOut(600, function () {
      $(".btn-images-wr").css("margin", "0");
      $(".section-hero-banner .bg-player").YTPPause();//pause bgPlayer
    });
    $(".loading").removeClass("loading");
    if(windowW<480){
      $(".overlay-content").css("transform", "translate3d(0,34vh,0");
    }else{
      $(".overlay-content").css("transform", "translate3d(0,30vh,0");
    }
    
  });

  //capsule buttons PLAY
  $(".js-banner-trigger-show-play").on('click', function (event) {
    var ready = $("#main-video-target").hasClass("yt-ready");
    var windowW = parseInt(jQuery(window).width());

    //play video banner
    if(windowW>480){
      var vidId = $(this).attr("data-yt");
    }else{
      var vidId = $(this).attr("data-yt-mobile");
    }
    
    var vid = 'https://youtu.be/' + vidId;
    var ytData = "{videoURL:'"+vidId+"',containment:'#main-video', showControls:false, autoPlay:true, loop:false, mute:false, startAt:0, opacity:1, addRaster:false, quality:'default',stopMovieOnBlur:false,playOnlyIfVisible:true}"
    
    if(ready){
      console.log("player had been played before");
      $('#main-video-target').YTPChangeVideo({
        videoURL: vidId,
        startAt: 1,
        mute:false
      });
    }else{
      $("#main-video-target").attr("data-property",ytData)
      BannerPLayer(vidId);
    }
    
    //animation
    $(".hero-overlay-bg").removeClass("page-hidden").css("z-index", "0");
    $(".overlay-content-bg").fadeOut(1000);
    $(".overlay-content .context-wr,.overlay-content .btn-primary,.overlay-content .arrow-wr").fadeOut(600, function () {
      $(".btn-images-wr").css("margin", "0");
      $(".section-hero-banner .bg-player").YTPPause();//pause bgPlayer
      
    });
    $(".loading").removeClass("loading");
    if(windowW<480){
      $(".overlay-content").css("transform", "translate3d(0,34vh,0");
    }else{
      $(".overlay-content").css("transform", "translate3d(0,30vh,0");
    }
  });

}

function bannerBGDesktop(){
  var $target = $(".section-hero-banner .bg-player"),
  oldData = $target.data('desktop');

  $target.removeAttr('data-desktop').attr({ 'data-property': oldData });
}
function bannerBGMobile(){
  var $target = $(".section-hero-banner .bg-player"),
  oldData = $target.data('mobile');

  $target.removeAttr('data-mobile').attr({ 'data-property': oldData });
}

import 'scss/components/vendor/jquery.mb.YTPlayer.min.scss';
var YTPlayer = require("bundle-loader?lazy&name=YTPlayer!./vendor/jquery.mb.YTPlayer");
var Player, bgplayer;
var Player = $(".section-hero-banner .player");
var bgplayer = $(".section-hero-banner .bg-player");
var PlayerExist = Player.length;
var bgplayerExist = bgplayer.length;
YTPlayer(function (file) {
  console.log("hero banner video - YTPlayer");
  var windowW = parseInt(jQuery(window).width());

  if(windowW>480){
    bannerBGDesktop()
  }else{
    bannerBGMobile()
  }
  BannerVideoBG();
});


//banner video Player
function BannerPLayer(vidId) {
  console.log("BannerVideo == called");
  console.log(vidId);
  if (PlayerExist > 0) {
    $.mbYTPlayer.apiKey = "AIzaSyBNVFiNtidAooNyAkhqyrXbqwKFxf49EdM";
    window.onYouTubeIframeAPIReady = function () {
      console.log("onYouTubeIframeAPIReady");
      Player.YTPlayer({
        onReady: function () {
          console.log("banner player is ready");
          $("#main-video-target").addClass("yt-ready");
        }
      });
    };

    Player.YTPlayer({
      onReady: function () {
        console.log("banner player is ready");
        $("#main-video-target").addClass("yt-ready");
      }
    });

    console.log(Player);
    console.log("test test")
    
    Player.on("YTPStart YTPEnd YTPLoop YTPPause YTPBuffering", function (e) {
      console.log(e.type);
      if(e.type=="YTPEnd"){
        $('#promo').modal('show')
      }
    });

  }
}

//banner bg Video
function BannerVideoBG() {
  console.log("BannerVideo == called");

  

  
  if (bgplayerExist > 0) {
    $.mbYTPlayer.apiKey = "AIzaSyBNVFiNtidAooNyAkhqyrXbqwKFxf49EdM";
    window.onYouTubeIframeAPIReady = function () {
      console.log("onYouTubeIframeAPIReady");
     bgplayer.YTPlayer({
        onReady: function () {
          console.log("bg player is ready");
          $("#bg-video").addClass("yt-ready");
        }
      });
    };

    bgplayer.YTPlayer();

    console.log(bgplayer);
    

    bgplayer.on("YTPStart YTPEnd YTPLoop YTPPause YTPBuffering", function (e) {
      console.log(e.type);
    });

    bgplayer.on("YTPTime", function (e) {
      var currentTime = e.time;
      console.log(currentTime);
      if(currentTime==13){
        bgplayer.YTPSeekTo(0);
        
      }
    });

  }
}
