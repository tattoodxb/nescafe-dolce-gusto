import 'scss/components/slider-dual.scss';

$(".slider-dual .img-wr").on('click', function(e) {
	var thistarget = $(this);
	var notTarget = $(".slider-dual .img-wr").not(this);
	thistarget.toggleClass("center right").addClass("hold");
	notTarget.toggleClass("center right").addClass("hold");
	setTimeout(function(){
		$(".hold").removeClass("hold");
	}, 600);
});


$(".slider-dual").on('mouseenter', '.right:not(.hold)', function() {
	console.log("in");
	var thistarget = $(this);
	var notTarget = $(".slider-dual .img-wr").not(this);
	thistarget.removeClass("right").addClass("center hold");
	notTarget.removeClass("center").addClass("right hold");
	setTimeout(function(){
		$(".hold").removeClass("hold");
	}, 600);
});


$(".slider-dual").on('mouseenter', '.center:not(.hold)', function() {
	console.log("in");
	var thistarget = $(this);
	var notTarget = $(".slider-dual .img-wr").not(this);
	thistarget.removeClass("center").addClass("right hold");
	notTarget.removeClass("right").addClass("center hold");
	setTimeout(function(){
		$(".hold").removeClass("hold");
	}, 600);
});