

var Custom = require("bundle-loader?lazy&name=jquery.dotdotdot!./jquery.dotdotdot");
Custom(function(file){
    console.log("dotdotdot added");
    ellipsis();
});

    console.log("ellipsis");

    function ellipsis(){
        var windowW = parseInt(jQuery(window).width());
        var windowH = parseInt(jQuery(window).height());
        console.log("-------ellipsis function--------");

        var ie = (function (){
            if (window.ActiveXObject === undefined) return null;
            if (!document.querySelector) return 7;
            if (!document.addEventListener) return 8;
            if (!window.atob) return 9;
            if (!document.__proto__) return 10;
            return 11;
        })();

        var ieV = "ie-"+ie;


        
        if(windowW>480){
            console.log("-------ellipsis desktop ----------");
            $( ".ellipsis" ).each(function() {
                var target = $(this);
                var targetH = target.attr("data-line");
                var lineH = parseInt(target.css("line-height")) * targetH;
                var max = target.hasClass("max");
                console.log(targetH);
                console.log(lineH);

                if( ie !== null ){
                    var lineH = lineH + 2;
                }else{
                    var lineH = lineH + 2;
                }

                console.log(lineH);
                
                if(windowW<windowH){
                    console.log("portrait");
                    target.css({"max-height":lineH});
                }else if(max){
                    console.log("max");
                    target.css({"max-height":lineH});
                }else{
                    target.css({"max-height":lineH, "min-height":lineH});
                }          
                target.dotdotdot({
                
                    callback: function( isTruncated ) {
                        console.log(target);
                        target.addClass("isTruncated")
                    },
                    /* Function invoked after truncating the text.
                        Inside this function, "this" refers to the wrapper. */
                
                    ellipsis: "\u2026 ",
                    /* The text to add as ellipsis. */
                
                    height: null,
                    /* The (max-)height for the wrapper:
                        null: measure the CSS (max-)height ones;
                        a number: sets a specific height in pixels;
                        "watch": re-measures the CSS (max-)height in the "watch". */
                
                    keep: null,
                    /* jQuery-selector for elements to keep after the ellipsis. */
                
                    tolerance: 0,
                    /* Deviation for the measured wrapper height. */
                
                    truncate: "word",
                    /* How to truncate the text: By "node", "word" or "letter". */
                
                    watch: "window",
                    /* Whether to update the ellipsis: 
                        true: Monitors the wrapper width and height;
                        "window": Monitors the window width and height. */
                
                });
            });
        }else{
            console.log("-------ellipsis mobile ----------");
            $( ".ellipsis-xs" ).each(function() {
                var target = $(this);
                var targetH = target.attr("data-line");
                
                if(target.attr('data-xs-line')){
                    console.log("xs data line");
                    var targetH = target.attr("data-xs-line");
                }else{
                    console.log("xs data line not");
                }

                var lineH = parseInt(target.css("line-height")) * targetH;
                var max = target.hasClass("max");
                console.log(targetH);
                console.log(lineH);

                if( ie !== null ){
                    var lineH = lineH + 2;
                }else{
                    var lineH = lineH + 2;
                }

                console.log(lineH);
                
                if(max){
                    target.css({"max-height":lineH});
                }else{
                    target.css({"max-height":lineH, "min-height":lineH});
                }   
                
                target.dotdotdot({
                
                    callback: function( isTruncated ) {
                        console.log(target);
                    },
                    /* Function invoked after truncating the text.
                        Inside this function, "this" refers to the wrapper. */
                
                    ellipsis: "\u2026 ",
                    /* The text to add as ellipsis. */
                
                    height: null,
                    /* The (max-)height for the wrapper:
                        null: measure the CSS (max-)height ones;
                        a number: sets a specific height in pixels;
                        "watch": re-measures the CSS (max-)height in the "watch". */
                
                    keep: null,
                    /* jQuery-selector for elements to keep after the ellipsis. */
                
                    tolerance: 0,
                    /* Deviation for the measured wrapper height. */
                
                    truncate: "word",
                    /* How to truncate the text: By "node", "word" or "letter". */
                
                    watch: "window",
                    /* Whether to update the ellipsis: 
                        true: Monitors the wrapper width and height;
                        "window": Monitors the window width and height. */
                
                });
            });
        }
    }

