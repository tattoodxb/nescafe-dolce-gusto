//import '../../../../node_modules/swiper/dist/css/swiper.css';
//import '../../../../node_modules/swiper/dist/css/swiper.css';
//import Swiper from 'swiper';

import 'scss/components/vendor/jquery-ui.min.scss';
var jquery_ui = require("bundle-loader?lazy&name=jquery_ui!./jquery-ui.min");
jquery_ui(function(file){
    console.log("jquery_ui");
});

import 'scss/components/vendor/swiper.scss';
//import Swiper from 'swiper';
import Swiper from './swiper.js';

import 'scss/components/slider-swiper.scss';

var Custom = require("bundle-loader?lazy&name=custom!../custom");
Custom(function(file){
    console.log("custom from slide-swiper");
    $('.section-features-main-navtabs a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
        var active = e.target // newly activated tab
        var prev = e.relatedTarget // previous active tab
        var curr = $(e.target).attr("href").substr(1);
        //console.log(curr);        
        //console.log(active);
        //console.log(prev);
        
        var flavours = $(".main-tab-content .tab-pane.active").hasClass("tab-flavours")
        var coffeeMachines = $(".main-tab-content .tab-pane.active").hasClass("tab-coffee-machines")
        if(flavours){
            swipeFlavours();
        }else if(coffeeMachines){
            swipeCoffeMachines()
        }
        //$('.mobile-select-tabs').selectpicker('val', curr);
    });
});


var flavoursRun = 1;
function swipeFlavours(){
    console.log("initSwipe called");
    
    if(flavoursRun==1){
        console.log("initSwipe run");
        flavoursRun = 0;
        var swiperCon = $(".swiper-container")
        var swiperSlide = swiperCon.find('.swiper-slide');

        if ( swiperSlide.length > 1 ) {
            console.log("slide > 1");
            if(swiperCon.hasClass("flavours-slider")){
                console.log("flavours slider --- 1");
                var flavourSlide = ".flavours-slider",
                Flavoursoptions = {
                    loop: true,
                    autoplay: false,
                    watchOverflow: true,
                    centeredSlides:true,
                    
                    effect: 'fade',
                    fadeEffect: {
                        crossFade: true
                    },
                    speed: 500,
                    /*navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev'
                    },*/
                    pagination: {
                        el: '.swiper-pagination',
                        clickable:true,
                    },
                    direction: 'horizontal',
                    slidesPerView: 1,
                    spaceBetween:0,
                    grabCursor: true,
                    watchSlidesProgress: true,
                    lazy: true,
                    lazy: {
                        loadPrevNext: true,
                        loadPrevNextAmount: 3,
                    },
                    preloadImages: false,
                    on: {
                        init: function() {
                            console.log("init");
                            //this.autoplay.stop();

                            let swiper = this,
                            currentSlide = swiper.$el.find(".swiper-slide-active"),
                            duplicate = swiper.$el.find(".swiper-slide-duplicate"),
                            descs = swiper.$el.find(".desc"),
                            currentSlideActive = swiper.slides[swiper.activeIndex];

                            $(currentSlideActive).addClass("slide-focused slide-seen");
                            $(duplicate).addClass("slide-focused slide-seen");

                            //custom nav 
                            $(".flavours-slider-custom-nav .swiper-button-prev").on('click', function(e){
                                swiper.slidePrev();
                            });
                            $(".flavours-slider-custom-nav .swiper-button-next").on('click', function(e){
                                swiper.slideNext();
                            });
                        },
                        lazyImageReady: function() {
                            console.log("lazyImageReady");
                            $(".loading").removeClass("loading").addClass("loaded");
                            //this.autoplay.start();
                        },
                        imagesReady: function() {
                            console.log("imagesReady");
                            $(".loading").removeClass("loading").addClass("loaded");
                            //this.autoplay.start();
                        },
                        slideChange: function() {
                            console.log("slideChange");
                        },
                        slideChangeTransitionStart: function() {
                            console.log("slideChangeTransitionStart");
                            let swiper = this,
                            slides = swiper.$el.find(".swiper-slide"),
                            descs = swiper.$el.find(".desc"),
                            currentSlide = swiper.$el.find(".swiper-slide-active"),
                            currentSlideActive = swiper.slides[swiper.activeIndex];
                            
                            slides.removeClass("slide-focused");
                            //descs.removeClass("show");

                            $(currentSlideActive).addClass("slide-focused slide-seen");
                            var titleW = $(currentSlideActive).find(".prime-inner").attr("data-width");
                            var descW = $(currentSlideActive).find(" .desc").attr("data-width");

                            $(currentSlideActive).find(".info-wr").animate({"width":titleW},800);
                            $(currentSlideActive).find(".prime-inner").css({"min-height":"380px","width":titleW});
                            $(currentSlideActive).find(".desc-wr").animate({"width":descW},900);
                            $(currentSlideActive).find(".init").animate({"width":"0"},500);
                        },
                        slideChangeTransitionEnd: function() {
                            console.log("slideChangeTransitionEnd");

                        },
                        progress: function() {
                            console.log("progress");
                        },
                        touchStart: function() {
                            console.log("touchStart");
                        },
                        setTransition: function(speed) {
                            console.log("setTransition");
                        }
                    }
                }


                var swiper = new Swiper(flavourSlide, Flavoursoptions);
                
                //end of tabs flavour slider
            }
        } 

    }
        

}

var coffeMachinesRun = 1;
function swipeCoffeMachines(){
    console.log("initSwipe called");
    
    if(coffeMachinesRun==1){
        console.log("initSwipe run");
        coffeMachinesRun = 0;
        
        var swiperCon = $(".swiper-container")
        var swiperSlide = swiperCon.find('.swiper-slide');
        var space = parseInt($(window).width()) * .25;
        if ( swiperSlide.length > 1 ) {
            console.log("slide > 1");
            if(swiperCon.hasClass("coffee-machines-slider")){
                console.log("CoffeeMachines slider --- 1");
                var CoffeeMachineslide = ".coffee-machines-slider",
                CoffeeMachinesoptions = {
                    speed: 500,
                    initialSlide: 1,
                    loop: true,
                    autoplay: false,
                    watchOverflow: true,
                    centeredSlides:true,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev'
                    },
                    pagination: {
                        el: '.swiper-pagination',
                        clickable:true,
                    },
                    direction: 'horizontal',
                    slidesPerView: 'auto',
                    loopedSlides: 6,
                    spaceBetween:space,
                    grabCursor: true,
                    watchSlidesProgress: true,
                    lazy: true,
                    lazy: {
                        loadPrevNext: true,
                        loadPrevNextAmount: 3,
                    },
                    preloadImages: false,
                    on: {
                        init: function() {
                            console.log("init");
                            //this.autoplay.stop();

                            let swiper = this,
                            currentSlide = swiper.$el.find(".swiper-slide-active"),
                            duplicate = swiper.$el.find(".swiper-slide-duplicate"),
                            descs = swiper.$el.find(".desc"),
                            currentSlideActive = swiper.slides[swiper.activeIndex];

                            //$(currentSlideActive).addClass("slide-focused slide-seen");
                            //$(duplicate).addClass("slide-focused slide-seen");

                        },
                        lazyImageReady: function() {
                            console.log("lazyImageReady");
                            $(".loading").removeClass("loading").addClass("loaded");
                            //this.autoplay.start();
                        },
                        imagesReady: function() {
                            console.log("imagesReady");
                            $(".loading").removeClass("loading").addClass("loaded");
                            //this.autoplay.start();
                        },
                        slideChange: function() {
                            console.log("slideChange");
                        },
                        slideChangeTransitionStart: function() {
                            console.log("slideChangeTransitionStart");
                            let swiper = this,
                            slides = swiper.$el.find(".swiper-slide"),
                            descs = swiper.$el.find(".desc"),
                            currentSlide = swiper.$el.find(".swiper-slide-active"),
                            currentSlideActive = swiper.slides[swiper.activeIndex];
                            
                            slides.removeClass("slide-focused");
                            //descs.removeClass("show");

                            //$(currentSlideActive).addClass("slide-focused slide-seen");
                            var titleW = $(currentSlideActive).find(".prime-inner").attr("data-width");
                            var descW = $(currentSlideActive).find(" .desc").attr("data-width");

                            //$(currentSlideActive).find(".info-wr").animate({"width":titleW},800);
                            //$(currentSlideActive).find(".prime-inner").css({"min-height":"380px","width":titleW});
                            //$(currentSlideActive).find(".desc-wr").animate({"width":descW},900);
                            //$(currentSlideActive).find(".init").animate({"width":"0"},500);
                        },
                        slideChangeTransitionEnd: function() {
                            console.log("slideChangeTransitionEnd");

                        },
                        progress: function() {
                            console.log("progress");
                        },
                        touchStart: function() {
                            console.log("touchStart");
                        },
                        setTransition: function(speed) {
                            console.log("setTransition");
                        }
                    }
                }
               

                var swiper = new Swiper(CoffeeMachineslide, CoffeeMachinesoptions);

                //end of tabs flavour slider
            }
        } 

    }
        

}
