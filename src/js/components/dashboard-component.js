import 'scss/components/hero-banner.scss';

console.log("dashboard component");

var Custom = require("bundle-loader?lazy&name=custom!./custom");
Custom(function(file){
    console.log("custom");

    $("input[type=file]").change(function () {
      var fieldVal = $(this).val();
    
      // Change the node's value by removing the fake path (Chrome)
      fieldVal = fieldVal.replace("C:\\fakepath\\", "");
        
      if (fieldVal != undefined || fieldVal != "") {
        $(this).next(".custom-file-label").attr('data-content', fieldVal);
        var target = $(this).parent().find(".input-file-label-text");
        if(target.length){
          console.log("text");
          target.text(fieldVal);
        }else{
          console.log("no text");
          $(this).next(".custom-file-label").text(fieldVal);
        }
        
      }
    
    });


    //forum dropdown categories		
  $(".toggle-dropdown-cat").click(function() {
    $(this).toggleClass("open");
    $(this).find(".caret").toggleClass("fa-rotate-180");
    $(".dropdown-cat").slideFadeToggle();
  });

  var windowW = parseInt(jQuery(window).width());
  var windowH = parseInt(jQuery(window).height());
  var navH = $("nav.navbar").outerHeight();
  var availH = windowH - navH;

  if(windowW>480){
    var partnerH = $('.match-wr').map(function() {
      return jQuery(this).height();
    }).get();
    var partnermaxH = Math.max.apply(null, partnerH);
    $('.match-wr').css("min-height",partnermaxH);


    //sticky
    console.log("sticky here");
    var h = parseInt($(".profile-menu-inner").css("height"));
    console.log(h);
    console.log(availH);
    if(h<availH){
      window.onscroll = function() {stickyTrigger()};

      var navbar = $(".profile-menu");
      var sticky = navbar.offset().top;
      var w = $(".profile-menu-inner").css("width");
      
      $(".profile-menu-inner").css("width", w);

      function stickyTrigger() {
        console.log("sticky");
        if (window.pageYOffset >= sticky) {
          console.log("addClass");
          navbar.addClass("sticky")
        } else {
          navbar.removeClass("sticky");
          console.log("removeClass");
        }
      }
    }
  }
  
  
  
});

