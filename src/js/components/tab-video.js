import 'scss/components/vendor/jquery.mb.YTPlayer.min.scss';
var YTPlayer = require("bundle-loader?lazy&name=YTPlayer!./vendor/jquery.mb.YTPlayer");
var playerTabExist = $(".sub-tab-content .tab-player").length;
var playerReady = $(".body-tabs iframe.playerBox").length;

YTPlayer(function(file){
  console.log("tab videos YTPlayer");

  TabVideo()
});


function TabVideo(){
  console.log("TabVideos == called");
  if(playerTabExist>0){

    //adjust video bg size
    var vidW = parseInt($(".sub-tab-content").css("width"));
    var vidH = (9/ 16) * vidW;
    $(".tab-content-video-bg").css("height",vidH);
    

    $.mbYTPlayer.apiKey = "AIzaSyBNVFiNtidAooNyAkhqyrXbqwKFxf49EdM";
    window.onYouTubeIframeAPIReady = function () {
      console.log("onYouTubeIframeAPIReady");          
    };

    //init all videos
    $(".sub-tab-content .tab-player").each(function(){
      var el = $(this)
      el.YTPlayer({
        onReady: function (player) {
          console.log(el);
          console.log("tab player is ready");
          el.addClass("yt-ready");
        }
      });
    });

    //tab switch pause video
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
      var active = e.target // newly activated tab
      var prev = e.relatedTarget // previous active tab
      console.log(active);
      console.log(prev);

      $(".tab-player").each(function(){
        $(this).YTPPause();
      });

    })
  }
}