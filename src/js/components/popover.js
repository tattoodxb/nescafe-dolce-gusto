function popOver(){
  
  $.fn.isChildOf = function(element){return $(element).has(this).length > 0;}
  $( "body" ).click(function( e ) {
      console.log(e.target);
      var curtarget = $(event.target);
      if ( !curtarget.isChildOf('.popover') && !curtarget.hasClass("date") && !curtarget.hasClass("active")) {
          console.log("target");
          $('[data-toggle="popover"]').popover('hide');
      }
  });

  $('[data-toggle="popover"]').popover({
    html:true,
    trigger: 'click'
  });
  $('[data-toggle="popover"]').on('show.bs.popover', function () {
    var target = $(this);
    $('[data-toggle="popover"]').not(target).popover('hide');
  });
  $('[data-toggle="popover"]').on('shown.bs.popover', function () {
    var target = $(this);
    target.addClass("showing")
  })
  $('[data-toggle="popover"]').on('hide.bs.popover', function () {
    var target = $(this);
    target.removeClass("showing")
  });
  $("body").on('click', '[data-toggle="popover"]', function(e) {
    var target = $(this);
    var shown = $(this).hasClass("showing");
    console.log(target);
    if(shown){
      target.popover('hide');
    }
    //
  });
}

popOver();