import 'scss/components/read-more.scss';

var Custom = require("bundle-loader?lazy&name=custom!./custom");
Custom(function(file){
    console.log("-------- read more call");
    ReadMore();
});


function ReadMore(){
  var windowW = parseInt(jQuery(window).width());
  var windowH = parseInt(jQuery(window).height());
  console.log("-------read more each function--------");
  
 
    $( ".read-more-wr" ).each(function() {
      var target = $(this);
      var initial = target.attr("data-desc");
      var all = target.find("p");
      var total = all.length;
      
      if(total>initial){
        all.addClass("d-none");
        
        all.each(function(i){
          var el = $(this);
          var count = i+1;
          if(count<=initial){
            el.removeClass("d-none");
            console.log(i);
            if(count==initial){
              console.log(count);
              console.log(initial);
              var hiddens = target.find(".d-none");
              hiddens.wrapAll( '<div class="toggle"></div>' );
              hiddens.removeClass("d-none");
              target.find(".toggle").slideFadeToggle();
            }
          }
        });

        var readmore = '<button class="btn btn-primary mt-2 mb-4 mt-sm-2 read-btn" type="button"><span>Read More</span></button>';

        target.append(readmore);

      }

    });
    var read = 1;
    $(".read-btn").on("click",function(e){
      e.preventDefault();
      var readbtn = $(this);
      var parentWr = readbtn.parents(".read-more-wr");
      var target = parentWr.find(".toggle");
      console.log("read more triggered");

      target.slideFadeToggle();
      if(read==1){
        readbtn.text("Read Less");
        read = 0;
      }else{
        readbtn.text("Read More");
        read = 1;
      }
    });
 
}