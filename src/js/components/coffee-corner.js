$(".strips__strip").on('click', function (event) {
   
});

var Expand = (function() {
    var tile = $('.strips__strip');
    var tileLink = $('.strips__strip > .strip__content');
    var tileText = tileLink.find('.strip__inner-text');
    var stripClose = $('.strip__close');
    
    var expanded  = false;
  
    var open = function() {
        
      var tile = $(this).parent();
  
        if (!expanded) {
          tile.addClass('strips__strip--expanded');
          // add delay to inner text
          tileText.css('transition', 'all .5s .3s cubic-bezier(0.23, 1, 0.32, 1)');
          stripClose.addClass('strip__close--show');
          stripClose.css('transition', 'all .6s .4s cubic-bezier(0.23, 1, 0.32, 1)');
          expanded = true;

          var el = tile;
          var not = $(".strips__strip").not(el);
          var img = el.find(".hotspot-img");
          var pLeft = parseInt(img.position().left)/2;
          var pTop = img.position().top;
          var imgH = img.css("height");
          var imgW = img.css("width");
          var hotSpot = el.find(".hotspot-elements");
          console.log(img);
          console.log(pLeft);
          console.log(pTop);
          console.log(imgH);
          console.log(imgW);
          console.log(el);
          console.log(not);
          not.css("z-index","1");
          el.css("z-index","9");
          hotSpot.css({"width":imgW,"height":imgH,"left":pLeft,"top":pTop});

        } 
      };
    
    var close = function() {
      if (expanded) {
        tile.removeClass('strips__strip--expanded');
        // remove delay from inner text
        tileText.css('transition', 'all 0.15s 0 cubic-bezier(0.23, 1, 0.32, 1)');
        stripClose.removeClass('strip__close--show');
        stripClose.css('transition', 'all 0.2s 0s cubic-bezier(0.23, 1, 0.32, 1)')
        expanded = false;
      }
    }
  
      var bindActions = function() {
        tileLink.on('click', open);
        stripClose.on('click', close);
      };
  
      var init = function() {
        bindActions();
      };
  
      return {
        init: init
      };
  
    }());
  
  Expand.init();

 



//hotspot

$('.hotspot-p').each(function(){
	
	var $this = $(this),
			top = $this.data('top'),
			left = $this.data('left');
	
	$this.css({
		top: top + "%",
		left: left + "%"
	});
	$this.find(".hotspot").addClass('is-visible');
	
});

$('.hotspot').each(function(){
    var parent = $(this).parent().hasClass("hotspot-p");
    if(parent==false){
        var $this = $(this),
                top = $this.data('top'),
                left = $this.data('left');
        
        $this.css({
            top: top + "%",
            left: left + "%"
        })
        .addClass('is-visible');
    }
});



$('.hotspot').on('click', function(e){
	
	var text = $(this).data('text');
	
	if(!$(this).hasClass('is-active'))
	{
		$(this).parents('.contain').find('.hotspot').removeClass('is-active');
		$(this).addClass('is-active');
		$(this).parents('.contain').find('.hotspots-label').html( '<strong>' + $(this).text() + '</strong> <span>' + text + '</span>' ).addClass('is-visible');
	}
	else
	{
		$(this).removeClass('is-active');
		$(this).parents('.contain').find('.hotspots-label').html( '<strong>' + $(this).text() + '</strong> <span>' + text + '</span>' ).removeClass('is-visible');	
	}
	
	e.preventDefault();
});

$(".hotspots-label").on('click', function(e){
	$(this).removeClass('is-visible');
	$(this).parents('.contain').find('.hotspot').removeClass('is-active');
	e.preventDefault();
});