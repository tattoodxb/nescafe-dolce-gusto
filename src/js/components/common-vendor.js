//var unveilhooks = require("bundle-loader?lazy&name=unveilhooks!vendor/ls.unveilhooks");

/*var lazySizes = require("bundle-loader?lazy&name=lazysizes!vendor/lazysizes-unveilhooks");
lazySizes(function(file){
    console.log("lazySizes");
});*/

//import 'lazysizes/plugins/respimg/ls.respimg';
//import 'lazysizes/plugins/optimumx/ls.optimumx';
var unveilhooks = require("bundle-loader?lazy&name=unveilhooks!lazysizes/plugins/unveilhooks/ls.unveilhooks.min");
var lazysizes = require("bundle-loader?lazy&name=lazysizes!lazysizes");
//var global = require("bundle-loader?lazy&name=global!./global");
unveilhooks(function(file){
    console.log("unveilhooks");

    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.init = true;
    lazySizesConfig.loadMode = 1;
        
    lazysizes(function(file){
        console.log("lazysizes");
        

        $(document).on('lazybeforeunveil', function(){
            console.log("lazybeforeunveil");
        });

        $(document).on('lazyloaded', function(e){
            //console.log("lazyloaded event");
            var el = e.target;
            var target = $(el);
            var captrigger = $(el).hasClass("cap-trigger-img");
            var delay = target.attr("data-delay");
            //console.log(delay);
            if(captrigger){
                if(delay!=undefined){
                    showCaps(target,delay);
                }else{
                    showCaps(target);
                }
            }
            
            
        });


       
      
        
    });
    
    
});

function showCaps(target,delay){
    //console.log("showCaps");
    //console.log(target);
    //console.log(delay);
    target.parent().addClass("animated bounceIn delay-"+delay);
}


/*var WOWjs = require("bundle-loader?lazy&name=wow!./vendor/WOW");
WOWjs(function(file){
    console.log("wow");
    var wow = new WOW(
        {
          boxClass:     'wow',      // animated element css class (default is wow)
          animateClass: 'animated', // animation css class (default is animated)
          offset:       0,          // distance to the element when triggering the animation (default is 0)
          mobile:       true,       // trigger animations on mobile devices (default is true)
          live:         true,       // act on asynchronously loaded content (default is true)
          callback:     function(box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
          },
          scrollContainer: null // optional scroll container selector, otherwise use window
        }
      );
      wow.init();
});*/


var InitSvgInline = require("bundle-loader?lazy&name=jquery-svg-to-inline-min!./vendor/jquery-svg-to-inline-min");
InitSvgInline(function(file){});

var Custom = require("bundle-loader?lazy&name=custom!./custom");
Custom(function(file){
    console.log("custom");
});

var read_more = require("bundle-loader?lazy&name=read-more!./read-more");
read_more(function(file){
    console.log("read_more");
});


import fontawesome from '@fortawesome/fontawesome'
import {faFacebook, faFacebookf, fatwitter, fainstagram, falinkedin, fayoutube,faTelegramPlane} from '@fortawesome/fontawesome-free-brands'
import { faCalendar, faClock, faIdBadge, faIdCard } from '@fortawesome/fontawesome-free-regular'

import faMapMakerAlt from '@fortawesome/fontawesome-free-solid/faMapMarkerAlt'
import faUser from '@fortawesome/fontawesome-free-solid/faUser'
import faUnlock from '@fortawesome/fontawesome-free-solid/faUnlock'
import faEnvelope from '@fortawesome/fontawesome-free-solid/faEnvelope'
import faPlus from '@fortawesome/fontawesome-free-solid/faPlus'
import faEdit from '@fortawesome/fontawesome-free-solid/faEdit'
import faPhone from '@fortawesome/fontawesome-free-solid/faPhone'
import faHandHoldingHeart from '@fortawesome/fontawesome-free-solid/faHandHoldingHeart'
import faHandHoldingUsd from '@fortawesome/fontawesome-free-solid/faHandHoldingUsd'
import faCheck from '@fortawesome/fontawesome-free-solid/faCheck'
import faSignOutAlt from '@fortawesome/fontawesome-free-solid/faSignOutAlt'
import faSignInAlt from '@fortawesome/fontawesome-free-solid/faSignInAlt'
import faCamera from '@fortawesome/fontawesome-free-solid/faCamera'


// Add the icon to the library so you can use it in your page
fontawesome.library.add(faUser,faFacebook,faTelegramPlane,faClock,faIdBadge,faIdCard,faCalendar,faMapMakerAlt,faUnlock,faEnvelope,faPlus,faEdit,faPhone,faHandHoldingHeart,faHandHoldingUsd,faCheck,faSignInAlt,faSignOutAlt,faCamera);