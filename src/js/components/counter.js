import 'scss/components/counter';


var jQwaypoints = require("bundle-loader?lazy&name=jQwaypoints!waypoints/jquery.waypoints");
var jQwaypointsinview = require("bundle-loader?lazy&name=jQwaypointsinview!waypoints/shortcuts/inview.min");

/*var counterup = require("bundle-loader?lazy&name=counterup!vendor/jquery.counterup");
jQwaypoints(function(file){
    console.log("jQwaypoints");
    counterup(function(file){
      console.log("counterup");
      counter();
  });
});

function counter(){
  $('.counter').counterUp({
    delay: 10,
    time: 1000,
    offset: 70,
    beginAt: 100,
});
}*/
var directCount = 1;
jQwaypoints(function(file){
  console.log("jQwaypoints");
  jQwaypointsinview(function(file){
    console.log("jQwaypointsinview");
    if($('.section-counter').length>0){
      console.log("section-counter");
      var inview = new Waypoint.Inview({
        element: $('.section-counter')[0],
        enter: function(direction) {
          console.log('Enter triggered with direction ' + direction);
            countUp();
        },
        entered: function(direction) {
          countUp();
        },
        exit: function(direction) {
          console.log('Exit triggered with direction ' + direction)
        },
        exited: function(direction) {
          console.log('Exited triggered with direction ' + direction)
        }
      });

    }else if($('.count-number').length>0){
      console.log("not section-counter");
      var inview = new Waypoint.Inview({
        element: $('.count-number')[0],
        enter: function(direction) {
          console.log('Enter triggered with direction ' + direction);
          if(directCount==1){
            CountV2();
            directCount = 0;
          }
        },
        entered: function(direction) {
          if(directCount==1){
            CountV2();
            directCount = 0;
          }
        },
        exit: function(direction) {
          console.log('Exit triggered with direction ' + direction)
        },
        exited: function(direction) {
          console.log('Exited triggered with direction ' + direction)
        }
      });

    }
  });
});

var now = 1;
function countUp(){
  console.log("--------------countUp")
  if(now==1){
    console.log("--------------counting")
    now = 0;
    setTimeout(function(){
      CountV2();
    }, 1000);
  }
}
/*
function CountV1(){
  $('.counter').each(function() {
    var $this = $(this),
      countTo = $this.attr('data-count');
    $({
      countNum: $this.text()
    }).animate({
      countNum: countTo
    }, {
      duration: 1000,
      easing: 'swing',
      step: function() {
        $this.text(Math.floor(this.countNum));
      },
      complete: function() {
        $this.text(this.countNum);
      }
    });
  });
}
*/

function CountV2(){

    $.fn.countTo = function (options) {
      options = options || {};
      
      return $(this).each(function () {
        // set options for current element
        var settings = $.extend({}, $.fn.countTo.defaults, {
          from:            $(this).data('from'),
          to:              $(this).data('to'),
          speed:           $(this).data('speed'),
          refreshInterval: $(this).data('refresh-interval'),
          decimals:        $(this).data('decimals'),
          format:        $(this).data('format'),
        }, options);
        
        // how many times to update the value, and how much to increment the value on each update
        var loops = Math.ceil(settings.speed / settings.refreshInterval),
          increment = (settings.to - settings.from) / loops;
        
        // references & variables that will change with each update
        var self = this,
          $self = $(this),
          loopCount = 0,
          value = settings.from,
          data = $self.data('countTo') || {};
        
        $self.data('countTo', data);
        
        // if an existing interval can be found, clear it first
        if (data.interval) {
          clearInterval(data.interval);
        }
        data.interval = setInterval(updateTimer, settings.refreshInterval);
        
        // initialize the element with the starting value
        render(value);
        
        function updateTimer() {
          value += increment;
          loopCount++;
          
          render(value);
          
          if (typeof(settings.onUpdate) == 'function') {
            settings.onUpdate.call(self, value);
          }
          
          if (loopCount >= loops) {
            // remove the interval
            $self.removeData('countTo');
            clearInterval(data.interval);
            value = settings.to;
            
            if (typeof(settings.onComplete) == 'function') {
              settings.onComplete.call(self, value);
            }
          }
        }
        
        function render(value) {
          var formattedValue = settings.formatter.call(self, value, settings);
          $self.html(formattedValue);
        }
      });
    };
    
    $.fn.countTo.defaults = {
      from: 0,               // the number the element should start at
      to: 0,                 // the number the element should end at
      speed: 1000,           // how long it should take to count between the target numbers
      refreshInterval: 100,  // how often the element should be updated
      decimals: 0,           // the number of decimal places to show
      formatter: formatter,  // handler for formatting the value before rendering
      onUpdate: null,        // callback method for every time the element is updated
      onComplete: null,       // callback method for when the element finishes updating
      format: true,
    };
    
    function formatter(value, settings) {
      return value.toFixed(settings.decimals);
    }
  
  
  
    // custom formatting example
    $('.count-number').data('countToOptions', {
      /*formatter: function (value, options) {
        return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
      }*/
      formatter: function (value,options) {
        console.log(options.format);
        if(options.format==false){
          return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
        }else{
          if (value >= 1000000000) {
            return (value / 1000000000).toFixed(0).replace(/\.0$/, '') + 'G';
          }
          if (value >= 1000000) {
            return (value / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
          }
          if (value >= 1000) {
            return (value / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
          }
          else{
            return value.toFixed(0);
          }
          return value;
        }
      }
    });
    
    // start all the timers
    $('.timer').each(count);  
    
    function count(options) {
    var $this = $(this);
    options = $.extend({}, options || {}, $this.data('countToOptions') || {});
    $this.countTo(options);
    }
  
}