var isotope = require("bundle-loader?lazy&name=isotope!./vendor/isotope.pkgd");

isotope(function(file){
  console.log("csrIndex load isotope")

  function globalIsotope(){
    var filterFns = {
      // show if number is greater than 50
      number200: function() {
        console.log("number200 ---- called");
        var number = $(this).find('.val-point').text();
        return parseInt( number, 10 ) < 200;
      },
      number400: function() {
        console.log("number400 ---- called");
        var number = $(this).find('.val-point').text();
        var val = parseInt( number, 10 );
        return val > 201 && val <= 400 ;
      },
      number600: function() {
        console.log("number600 ---- called");
        var number = $(this).find('.val-point').text();
        var val = parseInt( number, 10 );       
        return val > 401 && val <= 600 ;
      },
      number800: function() {
        console.log("number800 ---- called");
        var number = $(this).find('.val-point').text();
    var val = parseInt( number, 10 );       
    return val > 601 && val <= 800 ;
      },
      number1050: function() {
        console.log("number1050 ---- called");
        var number = $(this).find('.val-point').text();
        var val = parseInt( number, 10 );       
        return val > 801 && val <= 1050 ;
      },
      number2000: function() {
        console.log("number2000 ---- called");
        var number = $(this).find('.val-point').text();
        var val = parseInt( number, 10 );       
        return val > 2000;
      }
    };

    var unoDos = 1;
    function uno(){
      if(unoDos==1){
        var img = $('.grid .sm-figure .fig-inner-wr');
        var w = parseInt(img.innerWidth())+10;
        console.log(w);
        img.css("min-height", w);

        unoDos = 0;
      }    
    }
    
    // store filter for each group
    var filters = {};

    // init Isotope
    var $grid = $('.grid.grid-isotop').isotope({
      itemSelector: '.element-item',
      percentPosition: true,
      masonry: {
        columnWidth: '.grid-sizer'
      },
      filter: function() {
        console.log("isotope");
        var isMatched = true;
        var $this = $(this);
        console.log(unoDos);
        
        uno();
        
        
        

        for ( var prop in filters ) {
          var filter = filters[ prop ];
          // use function if it matches
          filter = filterFns[ filter ] || filter;
          // test each filter
          if ( filter ) {
            isMatched = isMatched && $(this).is( filter );
          }
          // break if not matched
          if ( !isMatched ) {
            break;
          }
        }
        return isMatched;
      }
    });

    // reveal all items after init
    var $items = $grid.find('.grid-item');
    $grid.addClass('is-showing-items')
      .isotope( 'revealItemElements', $items );

    $('.filters').on( 'change', function( event ) {
      console.log("onchange");
      var $this = $( event.target );
      
      var $buttonGroup = $this;
      var filterGroup = $buttonGroup.attr('value-group');

      console.log($buttonGroup);
      console.log(filterGroup);
      console.log($buttonGroup.val());
      // set filter for group
      filters[ filterGroup ] = $buttonGroup.val();
      console.log(filters[ filterGroup ]);
      // arrange, and use filter fn
      $grid.isotope();
    });

    var run = 1;
    $grid.on( 'layoutComplete',
      function( event, laidOutItems ) {
        console.log( 'Isotope layout completed on ' +
          laidOutItems.length + ' items' );

          if(run==1){
            setTimeout(function(){ 
              console.log("Hello"); 
              $grid.isotope();
            }, 10);
            run=0;
          }
            
      }
    );
    $grid.on( 'arrangeComplete',
      function( event, filteredItems ) {
        console.log( 'Isotope arrange completed on ' + filteredItems.length + ' items' );


      }
    );
  
  }
  globalIsotope();

});