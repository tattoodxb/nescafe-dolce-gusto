require('expose-loader?$!expose-loader?jQuery!jquery');
require('expose-loader?Tether!tether');
require("babel-polyfill");


var hero_banner = require("bundle-loader?lazy&name=hero-banner!./components/hero-banner");
var bootstrap_select = require("bundle-loader?lazy&name=bootstrap-select!./components/vendor/bootstrap-select");

hero_banner(function(file){});

var windowW = parseInt(jQuery(window).width());
if(windowW<481){
  var scrollOverflowOPT = true;
}else{
  var scrollOverflowOPT = false
}
var mobileTableSelect = true;
//import '../../node_modules/fullpage.js/vendors/scrolloverflow';
import 'scss/components/vendor/fullpage';
import fullpage from 'fullpage.js';
var fullPageInstance = new fullpage('#fullpage', {
  licenseKey: '40179669-F72C4345-BF22459A-5E2673BD',
  anchors: ['firstPage', 'secondPage', 'thirdPage'],
  //navigation:true,
  //navigationTooltips: ['Page 1', 'Page 2', 'Any text!'],
  //Custom selectors
  sectionSelector: '.fp-section ',
  slideSelector: '.fp-slide',
  menu: '.floating-fp-menu',
  normalScrollElements:'.content-parent-wr',
	/*scrollOverflow: scrollOverflowOPT,
  scrollOverflowReset: false,
  scrollOverflowOptions: {
      probeType: 3,
      mouseWheelSpeed: 1000,
      bounceEasing: true,
      bounceTime: 3000,
  },*/
  responsiveWidth:480,
  lazyLoading:false,

  //use to initialize other plugins or fire any code which requires the document ready
  afterRender: function(){
    console.log("afterRender fullpage");
    windowW = parseInt(jQuery(window).width());

    hero_banner(function(file){});

    //init bootstrap select for mobile
    if(windowW<480){
      if(mobileTableSelect){
        mobileTableSelect = false;
        bootstrap_select(function(file){
          console.log("bootstrap-select");
          if($('.mobile-select-tabs').length){
            $('.mobile-select-tabs').selectpicker({});
            features_mobile_select(function(file){});
          }
        });
      }
    }
    
  },
  afterResize: function(width, height){
    console.log("afterResize fullpage");
    windowW = parseInt(jQuery(window).width());
	},

  //This callback is fired once the user leaves a section, in the transition to the new section. 
  onLeave: function(origin, destination, direction){
    console.log("onLeave fullpage");
		var leavingSection = this;

    //detect if first section is next
    if(destination.index != 0){
      $(".floating-menu-wr").addClass("not-banner");
		}else{
      $(".floating-menu-wr").removeClass("not-banner");
    }

    //detect if second section is next
    if(destination.index == 1){
      //tab videos call
      tab_videos(function(file){});
      
      

    }
  },
  
  afterLoad: function (origin, destination, direction){
    console.log("afterLoad fullpage");
    var curr = destination.item;
    console.log(curr);
    $(curr).addClass("seen")
    AnimateShow();

    windowW = parseInt(jQuery(window).width());
    console.log(windowW);
    if(windowW < 481){
      console.log("mobile");
      fullpage_api.setAllowScrolling(true);
    }
    
    //init bootstrap select for mobile
    if(windowW<480){
      if(mobileTableSelect){
        mobileTableSelect = false;
        bootstrap_select(function(file){
          console.log("bootstrap-select");
          if($('.mobile-select-tabs').length){
            $('.mobile-select-tabs').selectpicker({});
            features_mobile_select(function(file){});
          }
        });
      }
    }

    //detect if second section is next
    if(destination.index == 1){
      console.log("second section is now");
      setTimeout(function(){ 
        AOS.refresh();
        console.log("second section is now 2");
      }, 1000);

      
    }

    if(destination.index == 2){
      console.log("last section");
      if(!$(curr).hasClass("last-shown")){
        $(curr).addClass("last-shown");
        console.log("has no last shown");
      }else{
        console.log("has last shown");
      }
      
    }
  },

  

});
/*console.log("------ JS -------");
console.log(fullpage);

console.log("------ $.fn.fullpage -------");
console.log($.fn.fullpage); */
import AOS from 'aos';
AOS.init();

$.fn.extend({
  animateCss: function(animationName, callback) {
    var animationEnd = (function(el) {
      var animations = {
        animation: 'animationend',
        OAnimation: 'oAnimationEnd',
        MozAnimation: 'mozAnimationEnd',
        WebkitAnimation: 'webkitAnimationEnd',
      };

      for (var t in animations) {
        if (el.style[t] !== undefined) {
          return animations[t];
        }
      }
    })(document.createElement('div'));

    this.addClass('animated ' + animationName).one(animationEnd, function() {
      $(this).removeClass('animated ' + animationName);
      console.log("removeClass");
      if (typeof callback === 'function') callback();
    });

    return this;
  },
});

function AnimateShow(){
  $(".seen .animated").each(function(){
    var el = $(this);
    var animate = el.attr("data-animate");
    //el.addClass(animate);
    el.animateCss(animate);
  });
}

//import 'bootstrap' // import bootstrap required js
import 'scss/common/bootstrap.scss'; // import bootstrap required css // import bootstrap required css
import 'scss/common/fonts';

import 'scss/common/nav.scss';
import 'scss/components/hero-banner.scss';
import 'scss/common/animation.scss';
import 'scss/common/footer.scss';

import 'scss/components/features.scss';
import 'scss/components/coffee-corner.scss';

//var page = require("bundle-loader?lazy&name=page!./page");
/*page(function(file){
  console.log("page");

  
});*/

var boostrap = require("bundle-loader?lazy&name=bootstrap!bootstrap");
//var popover = require("bundle-loader?lazy&name=popover!./components/popover");


var CommonVender = require("bundle-loader?lazy&name=common-vendor!./components/common-vendor");
CommonVender(function(file){
  console.log("CommonVender");
});

//var hero_banner = require("bundle-loader?lazy&name=hero-banner!./components/hero-banner");
//hero_banner(function(file){});
var tab_videos = require("bundle-loader?lazy&name=tab_videos!./components/tab-video");

var sliderSwiper = require("bundle-loader?lazy&name=slider-swiper!./components/vendor/slider-swiper");
//var VideoPopUp = require("bundle-loader?lazy&name=video-popup!./components/video-popup");

var coffeeCorner = require("bundle-loader?lazy&name=coffee-Corner!./components/coffee-corner");

var nav = require("bundle-loader?lazy&name=nav!./components/nav");

var ScrollToTarget = require("bundle-loader?lazy&name=scroll-to-target!./components/scroll-to-target");

//forms
//var select_picker_comp = require("bundle-loader?lazy&name=select_picker_comp!./components/select-picker"); //same as bootstrap select
//var date_picker_comp = require("bundle-loader?lazy&name=date_picker_comp!./components/date-picker");

import 'scss/components/vendor/bootstrap-select';
//var bootstrap_select = require("bundle-loader?lazy&name=bootstrap-select!./components/vendor/bootstrap-select");

var features_mobile_select = require("bundle-loader?lazy&name=features-mobile-select!./components/features-mobile-select");

document.documentElement.className += 
(("ontouchstart" in document.documentElement) ? ' touch' : ' no-touch');

$(window).on("load",function(){
  console.log("home load");

  var ie = (function (){
      if (window.ActiveXObject === undefined) return null;
      if (!document.querySelector) return 7;
      if (!document.addEventListener) return 8;
      if (!window.atob) return 9;
      if (!document.__proto__) return 10;
      return 11;
  })();
  var ieV = "ie-"+ie;
  if( ie !== null ){
      $("html").addClass(ieV);
      $("html").addClass("ie");
  }

  boostrap(function(file){
    /*popover(function(file){
      console.log("popover");
    });*/
  });

  nav(function(file){});
  ScrollToTarget(function(file){});

  sliderSwiper(function(file){
  
  });
  coffeeCorner(function(file){
    console.log("coffeeCorner");
  });

 //VideoPopUp(function(file){});
  

  /*if($('.date-picker').length>0){
    date_picker_comp(function(file){});
  }*/

 
});









