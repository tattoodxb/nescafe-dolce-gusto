require('expose-loader?$!expose-loader?jQuery!jquery');
require('expose-loader?Tether!tether');


import 'scss/common/fonts';

import 'bootstrap' // import bootstrap required js
import common from 'scss/common/bootstrap.scss'; // import bootstrap required css

import 'scss/common/nav.scss';
import 'scss/components/banner.scss';
import 'scss/common/breadcrumbs.scss';
import 'scss/components/partners.scss';
import 'scss/common/footer.scss';

var CommonVender = require("bundle-loader?lazy&name=common-vendor!./components/common-vendor");
var nav = require("bundle-loader?lazy&name=nav!./components/nav");
var ScrollToTarget = require("bundle-loader?lazy&name=scroll-to-target!./components/scroll-to-target");

var hero_banner = require("bundle-loader?lazy&name=hero-banner!./components/hero-banner");
var sliderSwiper = require("bundle-loader?lazy&name=slider-swiper!./components/vendor/slider-swiper");
var VideoPopUp = require("bundle-loader?lazy&name=video-popup!./components/video-popup");
var SliderDual = require("bundle-loader?lazy&name=slider-dual!./components/vendor/slider-dual");
var MemberHover = require("bundle-loader?lazy&name=team-members!./components/team-members");

$(window).on("load",function(){
  console.log("common load")
  
  CommonVender(function(file){});
  nav(function(file){});
  ScrollToTarget(function(file){});

  
  sliderSwiper(function(file){
    hero_banner(function(file){});
  });
  VideoPopUp(function(file){});
  SliderDual(function(file){});
  MemberHover(function(file){});
});



console.log("common js end");