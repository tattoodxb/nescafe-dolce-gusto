$.validator.setDefaults( {
  submitHandler: function () {
    alert( "submitted!" );
  }
} );

$( document ).ready( function () {

  jQuery.validator.addMethod("alphanumeric", function(value, element) {
      return this.optional(element) || /^[\w.]+$/i.test(value);
  }, "Letters, numbers, and underscores only please");

  $('.csr-form .select-picker').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
    console.log(clickedIndex);
    console.log(isSelected);
    console.log(previousValue);
    $(this).valid();
  });
  
  
  $( ".login-form" ).validate( {
    rules: {
     
      email: {
        required: true,
        minlength: 2
      },
      password: {
        required: true,
        minlength: 5
      },
    },
    messages: {
      username: {
        required: "Please enter a username",
        minlength: "Your username must consist of at least 2 characters"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
    },
    errorElement: "div",
    errorPlacement: function ( error, element ) {
      // Add the `invalid-feedback` class to the error element
      error.addClass( "invalid-feedback" );

      if ( element.prop( "type" ) === "checkbox" ) {
        error.insertAfter( element.next( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      var el = $( element );
      var inputGroup = el.parent();
      var inputGroupReal = inputGroup.hasClass("input-group");
      if(inputGroupReal){
        $( element ).addClass( "is-invalid" ).removeClass( "is-valid-" );
        inputGroup.addClass( "is-invalid" ).removeClass( "is-valid-" );
      }else{
        $( element ).addClass( "is-invalid" ).removeClass( "is-valid-" );
      }
      
    },
    unhighlight: function (element, errorClass, validClass) {
      var el = $( element );
      var inputGroup = el.parent();
      var inputGroupReal = inputGroup.hasClass("input-group");
      if(inputGroupReal){
        $( element ).addClass( "is-valid-" ).removeClass( "is-invalid" );
        inputGroup.addClass( "is-valid-" ).removeClass( "is-invalid" );
      }else{
        $( element ).addClass( "is-valid-" ).removeClass( "is-invalid" );
      }
    }
  } );

  $( ".csr-form" ).validate( {
    ignore: ":hidden",
    rules: {
      comp_name: "required",
      trade_num: {
        required: true,
        alphanumeric: true
      },
      lic_auth: "required",
      lic_exp_date: "required",

      add_1: "required",
      //add_2: "required",
      po_box: "required",
      emirate: "required",

      contact_pers: "required",
      contact_desig: "required",
      contact_tel: {
        required: true,
        digits: true
      },
      /*contact_fax: {
        required: true,
        digits: true
      },*/
      contact_mob: {
        required: true,
        digits: true
      },
      contact_email: {
        required: true,
        email: true
      },
    },
    messages: {
      firstname: "Please enter your firstname",
      lastname: "Please enter your lastname",
      username: {
        required: "Please enter a username",
        minlength: "Your username must consist of at least 2 characters"
      },
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long"
      },
      confirm_password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 5 characters long",
        equalTo: "Please enter the same password as above"
      },
      email: "Please enter a valid email address",
      agree: "Please accept our policy"
    },
    errorElement: "div",
    errorPlacement: function ( error, element ) {
      // Add the `invalid-feedback` class to the error element
      error.addClass( "invalid-feedback" );

      if ( element.prop( "type" ) === "checkbox" ) {
        error.insertAfter( element.next( "label" ) );
      } else {
        error.insertAfter( element );
      }
    },
    highlight: function ( element, errorClass, validClass ) {
      var el = $( element );
      var selectPicker = el.hasClass("select-picker");
      if(selectPicker){
        el.parent().find(".dropdown-toggle").addClass( "is-invalid" ).removeClass( "is-valid-" );
      }else{
        el.addClass( "is-invalid" ).removeClass( "is-valid-" );
      }
      
    },
    unhighlight: function (element, errorClass, validClass) {
      var el = $( element );
      var selectPicker = el.hasClass("select-picker");
      if(selectPicker){
        el.parent().find(".dropdown-toggle").addClass( "is-valid-" ).removeClass( "is-invalid" );
      }else{
        el.addClass( "is-valid-" ).removeClass( "is-invalid" );
      }
    }
  } );

} );