const path = require('path');
const glob = require('glob');
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
//const ExtractTextPlugin = require('extract-text-webpack-plugin'); //webpack4 bug
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
//const PurifyCSSPlugin = require('purifycss-webpack');
const PurgecssPlugin = require('purgecss-webpack-plugin');

/*const { ImageminWebpackPlugin } = require("imagemin-webpack");
const imageminGifsicle = require("imagemin-gifsicle");
const imageminOptipng = require('imagemin-optipng');
const imageminJpegtran = require('imagemin-jpegtran');*/

const ImageminPlugin = require( 'imagemin-webpack-plugin' ).default;
const { ImageminWebpackPlugin } = require("imagemin-webpack");
var imageminMozjpeg = require('imagemin-mozjpeg');
var imageminJpegtran = require('imagemin-jpegtran');
var imageminPngquant = require('imagemin-pngquant');
var imageminOptipng = require('imagemin-optipng');
var imageminGifsicle = require("imagemin-gifsicle")
var imageminSvgo = require('imagemin-svgo');

const HtmlWebpackInlineSVGPlugin = require('html-webpack-inline-svg-plugin');

const CopyWebpackPlugin = require('copy-webpack-plugin')

const DEV_MODE = process.env.NODE_ENV === 'dev';

const PATHS = {
  src: path.join(__dirname, 'src')
}

const DIRFont = path.resolve(__dirname, "./src/fonts/");

var themecustomJS = path.resolve(__dirname, "js/custom.js");

var waypoints = path.resolve(__dirname, "src/js/components/waypoints.js");

module.exports = {
  devServer: {
    headers: {
      'Access-Control-Allow-Origin': '*'
    },
  },
  //devtool: DEV_MODE ? 'eval' : 'source-map',
  devtool: 'source-map',
  entry: {
    //common: path.resolve(__dirname, 'src/js/common.js'),
    home: path.resolve(__dirname, 'src/js/home.js'),
   

    //'font-awesome-sass-loader!./font-awesome.config.js', //old
    //"font-awesome-webpack!./font-awesome.config.js", updated
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/[name].js',
    chunkFilename: '[name].[id].js',
    //publicPath: 'http://10.160.6.61:8080/' // update with your local ip
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        //exclude: /node_modules/,
        exclude: [/node_modules/, /themecustomJS/,/waypoints/], //multiple exclude
        loader: 'babel-loader'
      },
      {
        test: /\.bundle\.js$/,
        use: {
          loader: 'bundle-loader',
          options: {
            lazy: true,      
          }
        }
      },
      /*{
        test: require.resolve('jquery'),
        use: [{
          loader: 'expose-loader',
          options: 'jQuery'
        },{
          loader: 'expose-loader',
          options: '$'
        }]
      },
      {
        test: require.resolve('tether'),
        use: [
          { loader: 'expose-loader', options: 'Tether' }
        ]
      },*/
      DEV_MODE ? {
        test: /\.(scss|css)$/,
        exclude: /node_modules/,
        use: [{
          loader: 'style-loader', // inject CSS to page
          options: {sourceMap: true},
        }, {
          loader: "css-loader", // translates CSS into CommonJS modules
          options: {
            sourceMap: true,
            alias: {
              'img': path.join(__dirname, 'src', 'assets/images'),
            }
          }
        }, {
          loader: 'postcss-loader', // Run post css actions
          options: {
            sourceMap: true,
            plugins: function () { // post css plugins, can be exported to postcss.config.js
              return [
                require('precss'),
                require('autoprefixer')
              ];
            }
          }
        }, {
          loader: 'sass-loader', // compiles Sass to CSS
          options: {sourceMap: true}
        }]
      }
      : 
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              root: '.',
              url: true,
              publicPath: '../',
              alias: {
                'img': path.join(__dirname, 'src', 'assets/images'),
              }
            }
          },
        ]
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              root: '.',
              url: false,
              publicPath: '../',
              alias: {
                'img': path.join(__dirname, 'src', 'assets/images'),
              }
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              url: false,
              publicPath: '../'
            }
          },
          {
            loader: 'sass-loader',
            options: {
              url: false,
              publicPath: '../'
            }
          }
        ]
      },
      { // too much conflict --------------------------------------------------------------------- need to be fix on project start
        test: /\.html$/,
        use: [ {
          loader: 'html-loader',
          options: {
            //attrs: ['img:src', 'img:data-src', 'link:href'],
            attrs: [':data-img-src'],
            interpolate: true,
            minimize: true,
            removeComments: false,
            collapseWhitespace: false,
            removeAttributeQuotes: false,
            //minimize: true,
            //removeComments: false,
            //collapseWhitespace: false
          }
        }]
      },
      /*{
        test: /\.less$/,
        loader: 'less-loader' // compiles Less to CSS
      },*/
      //font
      /*{
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              limit: 10000,
              mimetype: 'application/font-woff',
              name: "[name].[ext]",
              outputPath: 'fonts/',
              publicPath: 'fonts/',
              include: path.join(__dirname,'src')
            }
          }
        ]
      },
      {
        test: /\.(ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              outputPath: 'fonts/',
              publicPath: 'fonts/',
              name: "[name].[ext]",
              include: path.join(__dirname,'src')
            }
          }
        ]
      },*/
      {
        test: /\.(jpe?g|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/, 
        include: [
          path.resolve(__dirname, 'src/assets/fonts')
       ],
        loader: 'file-loader?name=[name].[ext]'
     },
       {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          //'file-loader?name=images/[name].[ext]',
          //'file-loader?name=[name].[ext]&outputPath=assets/images/&publicPath=assets/images/',
          {
            loader: 'file-loader',
            options: {
              limit: 10000,
              outputPath: 'assets/images/',
              publicPath: 'assets/images/',
              name: '[name].[ext]',
            },
          },
         /*{
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                progressive: true,
                quality: 65
              },
              svgo:{
                plugins: [
                  {
                    removeViewBox: false
                  },
                  {
                    removeEmptyAttrs: false
                  }
                ]
              },
              // optipng.enabled: false will disable optipng
              optipng: {
                enabled: false,
              },
              pngquant: {
                quality: '65-90',
                speed: 4
              },
              gifsicle: {
                interlaced: false,
              },
              // the webp option will enable WEBP
              webp: {
                quality: 75
              }
            }
         },*/
        ],
      },

    ]
  },
  resolve: {
    extensions: ['.js','.scss','.css'],
    alias: {
      'waypoints': path.resolve(__dirname, 'src/js/components/vendor/waypoints/lib'),
      'assets': path.join(__dirname, 'src', 'assets'),
      'img': path.join(__dirname, 'src', 'assets/images'),
      'imgs': path.resolve(__dirname, '../src/assets/images'),
      'fontsDIR': path.resolve(__dirname, 'src/assets/fonts'),
      'rootjs': path.resolve(__dirname, 'src/js/'),
      'vendor': path.resolve(__dirname, 'src/js/components/vendor/'),
      'scss': path.resolve(__dirname, 'src/scss'),
      'layouts': path.resolve(__dirname, 'layouts'),
      'wp_js': path.resolve(__dirname, 'js'),
      'wp_plugins': path.resolve(__dirname, '../../plugins'),
      '@fortawesome/fontawesome-free-solid$': '@fortawesome/fontawesome-free-solid/shakable.es.js'
    }
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Tether: 'tether'
    }),

    /*new HtmlWebPackPlugin({
      title: 'Custom template',
      template: path.join(__dirname, 'src', 'index.html'),
      minify: {
        collapseWhitespace: DEV_MODE ? false :  true
      },
      //excludeChunks: ['about'],
      chunks: ['home'],
      hash: true,
    }),*/
    /**/
    new HtmlWebPackPlugin({
      title: 'Home',
      filename: 'index.html',
      template: path.join(__dirname, 'src', 'index.html'),
      chunks: ['home'],
      hash: true,
    }),
    
    
    new HtmlWebpackInlineSVGPlugin(), // needs to be after HtmlWebPackPlugin

    //new ExtractTextPlugin('css/styles.css'),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "css/[name].css",
      chunkFilename: "css/[name].css",
    }),
    // Make sure this is after ExtractTextPlugin!
    //new PurgecssPlugin({
    //  paths: glob.sync(`${PATHS.src}/**/*`, { nodir: true }),
      //only: ['home', 'about', 'events', 'licensing_requirements', 'benefits_and_services', 'registration'],
    //  whitelist: ['collapsing','show','hide','explicit','open','close','*-visible','visible-*','modal','modal-backdrop','no-gutters','vc_column_container','vc_column-inner','wpb_wrapper'],
    //  keyframes:true,
    //  fontFace: true,
    //  whitelistPatterns: [/^(modal|swiper)/],
    //  whitelistPatternsChildren: [/^(modal|swiper)/]
    //}),
    
    /*new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, 'src/assets/images/'),
        to: path.resolve(__dirname, 'assets/images/')
      },
      {
        from: path.resolve(__dirname, 'src/assets/fonts/'),
        to: path.resolve(__dirname, 'assets/fonts/'),
        test: /([^/]+)\/(.+)\.svg$/,
        ignore: [ '*.eot','*.otf','*.ttf','*.woff','*.woff2' ]
      },
      {
        from: path.resolve(__dirname, 'src/assets/fonts/'),
        to: path.resolve(__dirname, 'dist/assets/fonts/'),
        ignore: [ '*.svg']
      },
    ]),

    new ImageminWebpackPlugin({
      name: "[path][name].[ext]",
      test: /\.(jpe?g|png|gif|svg)$/i,
      imageminOptions: {
        plugins: [
          imageminSvgo({
            plugins: [
              {
                removeViewBox: true
              },
              {
                removeTitle: true
              },
              {
                removeComments:true
              },
              {
                removeMetadata:true
              },
              {
                removeDesc:true
              },
              {
                removeEmptyAttrs:true
              },
              {
                cleanupIDs: true,
              },
              {
                addClassesToSVGElement: {
                  className: "svg-inline",
                },
              },
              {
                removeAttrs: {
                  attrs: 'svg:id'
                },
              }
            ]
          }),
          imageminGifsicle({
            interlaced: true,
            optimizationLevel: 3
          }),
          imageminOptipng({
            interlaced: true,
            optimizationLevel: 3
          }),
          imageminPngquant({
            quality: 100,
            speed: 4,
          }),
          imageminMozjpeg({
            quality: 65,
            progressive: true,
          }),
        ]
      },
    }),*/

    new CopyWebpackPlugin([
      { from: 'src/assets/images/', to: 'assets/images/',copyUnmodified: false },
      { from: 'src/assets/fonts/', to: 'assets/fonts/',copyUnmodified: false }
    ]),

    /*new ImageminPlugin({
      test: /\.(jpe?g|png|gif|svg)$/i,
      optipng: {
          optimizationLevel: 7,
      },
      pngquant: {
          quality: '65-90',
          speed: 4,
      },
      gifsicle: {
          optimizationLevel: 3,
      },
      svgo: {
          plugins: [{
              removeViewBox: false,
              removeEmptyAttrs: true,
          }],
      },
      jpegtran: {
          progressive: true,
      },
      plugins: [
          imageminMozjpeg({
              quality: 65,
              progressive: true,
          }),
      ],
  	}),*/
  
  
  ]
}
